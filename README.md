# GlobalNET3

This repository houses Version 3 of the GlobalNET platform.

GlobalNET is developed by CivicActions for the Defense Security Cooperation Agency (DSCA).

## Installation instructions

 1. Make sure you have Composer installed locally.
 2. Run `composer install` in the project root to download all 3rd party libraries.
 3. Copy default.settings.php to settings.php; add database settings in settings.php.
 4. To install Drupal site with default configuration, add `$settings['config_sync_directory'] = '../config/sync';` to settings.php, and run `drush site:install --existing-config`.
 5. To import test data, run `drush en gn3_test_data`.
